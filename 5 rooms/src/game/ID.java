package game;

public enum ID {

	Player(),
	Enemy(),
	Bullet(),
	Gun(),
	GUI(),
	Particle();
}
